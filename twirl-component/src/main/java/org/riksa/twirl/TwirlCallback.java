package org.riksa.twirl;

import android.graphics.Rect;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/2/13
 */
public interface TwirlCallback {
    void twirlUpdate(int pointerId, Rect bounds, double handleDistance, double angle, int rotations, double localAngle);

    void twirlRemoved(int pointerId, double angle, int rotations, double localAngle);

    void twirlAdded(int pointerId, double angle, int rotations, double localAngle);
}
