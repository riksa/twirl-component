package org.riksa.twirl;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/2/13
 */
public class TwirlControllerTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testFullRotationInsideFirst() throws Exception {
        assertEquals( 0, TwirlController.fullRotation(0d, 1d) );
        assertEquals( 0, TwirlController.fullRotation(1d, 0d) );
    }

    @Test
    public void testFullRotationInsideLast() throws Exception {
        assertEquals( 0, TwirlController.fullRotation(2*Math.PI-0.1d, 2*Math.PI-1d) );
        assertEquals( 0, TwirlController.fullRotation(2*Math.PI-1d, 2*Math.PI-0.1d) );
    }

    @Test
    public void testFullRotationLastToFirst() throws Exception {
        assertEquals( 1, TwirlController.fullRotation(2*Math.PI-0.1d, 0.1d) );
    }

    @Test
    public void testFullRotationFirstToLast() throws Exception {
        assertEquals( -1, TwirlController.fullRotation(0.1d, 2*Math.PI-0.1d) );
    }

    @Test
    public void testFullRotationFirstToSecond() throws Exception {
        assertEquals( 0, TwirlController.fullRotation(0.1d, 2) );
    }

    @Test
    public void testFullRotationSecondToFirst() throws Exception {
        assertEquals( 0, TwirlController.fullRotation(2, 0.1) );
    }


}
